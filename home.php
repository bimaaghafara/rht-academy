<section class="home">
    <div class="slider-top">
        <div class="container-fluid">
            <div class="container"> 
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-top1.jpg">
                    <img class="text img-responsive" src="images/slider-top1-text.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-top2.jpg">
                    <img class="text img-responsive" src="images/slider-top2-text.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-top3.jpg">
                    <img class="text img-responsive" src="images/slider-top3-text.png">
                </div>
                <!-- <div>RHT Academy will strive to ensure a high and consistent level of quality in all our programmes</div>
                <div>
                    our goal:
                    > To be a premier training centre in Singapore.
                    > To be a regional training hub in the region.
                    > To be a platform to exchange ideas and best practices.
                </div>
                <div> RHT Academy strives to impart knowledge and skills to participants in the form of Seminars and Courses
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container content">
            <div class="upcoming-events col-xs-12 col-sm-12 col-md-6 col-md-push-3">
                <div class="title">
                    <div class="star">
                        <i class="fa fa-star"></i>
                        <div class="dotted-line">_ _ _ _ _</div>
                    </div>
                    <div class="text">upcoming events</div>
                </div>
                <div class="wrapper row">
                    <div class="top">
                        <div class="date"> 27 november 2015 </div>
                        <div class="view-detail"> <a href="#"> view detail </a> </div>
                    </div>
                    <div class="bottom">
                        <div class="img-container col-sm-3">
                            <div class="img">
                                <img class="img-responsive" src="images/upcoming-event.jpg">
                            </div>
                        </div>
                        <div class="title-container col-sm-9">
                            <div class="title">
                                introduction of krx listing of global firms: focus on dual listing for sgx companies
                            </div>
                        </div>
                        <div class="desc col-sm-12">As a Singapore-listed company, there are several platforms to raise capital, for example, dual listing in a foreign jurisdiction for the company's long term growth and development. The benefits include ready acces to these different equity markets in the Asia-Pasific region, widening the investor...</div>
                    </div>
                </div>
                <div class="wrapper row">
                    <div class="top">
                        <div class="date"> 27 november 2015 </div>
                        <div class="view-detail"> <a href="#" role="button"> view detail </a> </div>
                    </div>
                    <div class="bottom">
                        <div class="img-container col-sm-3">
                            <div class="img">
                                <img class="img-responsive" src="images/upcoming-event.jpg">
                            </div>
                        </div>
                        <div class="title-container col-sm-9">
                            <div class="title">
                                advanced contract law 2015: achieving successful outcomes
                            </div>
                        </div>
                        <div class="desc col-sm-12">Properly drafted and well managed contracts are pre-requisites for all successful business ventures, and more so in a challenging business environment. Join us at this Advanced Contract Law 2015 seminar, presented by leading industry experts from RHTLaw Taylor Wessing. They will uncover...</div>
                    </div>
                </div>
            </div>
            <div class="all-events col-xs-12 col-sm-6 col-md-3 col-md-pull-6">
                <div class="title">
                    <div class="calendar">
                        <i class="fa fa-calendar"></i>
                        <div class="dotted-line">_ _ _ _ _</div>
                    </div>
                    <div class="text">all events</div>
                </div>
                <div class="wrapper">
                    <div class="wrapper-date">
                        <div class="circle"></div>
                        <div class="date">
                            <div class="dd">22</div>
                            <div class="mm">oct</div>
                            <div class="yyyy">2015</div>
                        </div>
                    </div>
                    <div class="event-wrapper">
                        <div class="event-text">
                            syari'ah compliance seminars
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="wrapper-date">
                        <div class="circle"></div>
                        <div class="date">
                            <div class="dd">06</div>
                            <div class="mm">oct</div>
                            <div class="yyyy">2015</div>
                        </div>
                    </div>
                    <div class="event-wrapper">
                        <div class="event-text">
                            oil & gas joint operating agreement & negotiations in indonedia
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="wrapper-date">
                        <div class="circle"></div>
                        <div class="date">
                            <div class="dd">22</div>
                            <div class="mm">oct</div>
                            <div class="yyyy">2015</div>
                        </div>
                    </div>
                    <div class="event-wrapper">
                        <div class="event-text">
                            islamic finance: contemporary challenges 2015
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="wrapper-date">
                        <div class="circle"></div>
                        <div class="date">
                            <div class="dd">30</div>
                            <div class="mm">oct</div>
                            <div class="yyyy">2015</div>
                        </div>
                    </div>
                    <div class="event-wrapper">
                        <div class="event-text">
                            advanced loan documentation masterclass
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="wrapper-date">
                        <div class="circle"></div>
                        <div class="date">
                            <div class="dd">22</div>
                            <div class="mm">oct</div>
                            <div class="yyyy">2015</div>
                        </div>
                    </div>
                    <div class="event-wrapper">
                        <div class="event-text">
                            advanced loan documentation masterclass
                        </div>
                    </div>
                </div>
                <div class="pagination">
                    <a href="#">1</a> <span class="separator">-</span> 
                    <a href="#">2</a> <span class="separator">-</span> 
                    <a href="#">3</a> <span class="separator">-</span> 
                    <a href="#">4</a> 
                    <a href="#" class="pull-right"> older </a>
                </div>
            </div>
            <div class="twitter-posts col-xs-12 col-sm-6 col-md-3">
                <div class="title">
                    <div class="twitter">
                        <i class="fa fa-twitter"></i>
                    </div>
                    <div class="text">
                        <span class="username">RHT Academy</span>
                        <span class="follow"><br>Follow on Twitter</span>
                    </div>
                </div>
                <div class="wrapper row">
                    <div class="img-container col-xs-3">
                        <div class="img">
                            <img class="img-responsive" src="images/upcoming-event.jpg">
                        </div>
                    </div>
                    <div class="post-container col-xs-9">
                        <div class="top">
                            <span class="username"> @RHTAcademy </span>
                            <span class="date"> Jun 11 </span>
                        </div>
                        <div class="middle">
                            Expect an interactive session on June with Nizam Ismail, Director of RHT Compiliance Solutions. RSVP Now!
                            <a href="#"> goo.gl/V3fY7q </a>
                        </div>
                        <div class="bottom">
                            <i class="fa fa-reply"></i>
                            <i class="fa fa-retweet"></i>
                            <i class="fa fa-bookmark"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </div>
                    </div>
                </div>
                <div class="wrapper row">
                    <div class="img-container col-xs-3">
                        <div class="img">
                            <img class="img-responsive" src="images/upcoming-event.jpg">
                        </div>
                    </div>
                    <div class="post-container col-xs-9">
                        <div class="top">
                            <span class="username"> @RHTAcademy </span>
                            <span class="date"> Jun 11 </span>
                        </div>
                        <div class="middle">
                            Expect an interactive session on June with Nizam Ismail, Director of RHT Compiliance Solutions. RSVP Now!
                            <a href="#"> goo.gl/V3fY7q </a>
                        </div>
                        <div class="bottom">
                            <i class="fa fa-reply"></i>
                            <i class="fa fa-retweet"></i>
                            <i class="fa fa-bookmark"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </div>
                    </div>
                </div>
                <div class="wrapper row">
                    <div class="img-container col-xs-3">
                        <div class="img">
                            <img class="img-responsive" src="images/upcoming-event.jpg">
                        </div>
                    </div>
                    <div class="post-container col-xs-9">
                        <div class="top">
                            <span class="username"> @RHTAcademy </span>
                            <span class="date"> Jun 11 </span>
                        </div>
                        <div class="middle">
                            Expect an interactive session on June with Nizam Ismail, Director of RHT Compiliance Solutions. RSVP Now!
                            <a href="#"> goo.gl/V3fY7q </a>
                        </div>
                        <div class="bottom">
                            <i class="fa fa-reply"></i>
                            <i class="fa fa-retweet"></i>
                            <i class="fa fa-bookmark"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container text-center well-lg text-uppercase">
                <h1>- highlights -</h1>
        </div>
    </div>
    <div class="slider-bottom">
        <div class="container-fluid">
            <div class="container">
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom1.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom2.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom1.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom2.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom1.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom2.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom1.png">
                </div>
                <div class="wrapper"> 
                    <img class="bg img-responsive" src="images/slider-bottom2.png">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid testi">
        <div class="container text-center well-lg">
            <img class="quote" src="images/quote.png">   
            <h2>- TESTIMONIALS -</h2>
            <br>
            <p> "RHT Academy will strive to ensure a high and consistent level of quality in all of their programmers and aim to be a platform to value add to each professional and the relevant industries by the exchange of knowledge, information and ideas."<p>
            <br>
            <p> <small>- - - - -</small> </p>
            <h4>JOHN DOE </h4>
            <h5> DIRECTOR COMPANY ABC </h5>
        </div>
    </div>
    <div style="width:100%; height:15vw; max-height:300px;">
    </div>
</section>
