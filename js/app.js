
$(document).ready(function() {
	console.log('Initialized!');
});

$(document).ready(function(){
	$('.home .slider-top .container').slick({
		dots: false,
		infinite: true,
		speed: 2700,
		autoplay: true,
		autoplaySpeed: 4500,
		slidesToShow: 1,
		adaptiveHeight: true
	});
	if ($(window).width() < 540) {
		var sts=2;
	}else if ($(window).width() < 992)  {
		var sts=4;
	}else {
		var sts=6;
	}
	$('.home .slider-bottom .container').slick({
		dots: false,
		infinite: true,
		speed: 1800,
		autoplay: true,
		autoplaySpeed: 3600,
		slidesToShow: sts,
		slidesToScroll: 1,
		adaptiveHeight: true
	});
});