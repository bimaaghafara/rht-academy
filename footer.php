<footer class="footer">
    <div class="container-fluid">
        <div class="container">

            <div class="logo col-xs-12 col-sm-12 col-md-3">
                <img class="img-responsive" src="images/header-logo.png">
            </div>

            <div class="footer-menu col-xs-12 col-sm-12 col-md-6">
                <ul>
                    <li><a href="#" class="btn btn-link">about us</a></li>
                    <li><a href="#" class="btn btn-link">training list</a></li>
                    <li><a href="#" class="btn btn-link">our people</a></li>
                    <li><a href="#" class="btn btn-link">training calendar</a></li>
                    <li><a href="#" class="btn btn-link">our values</a></li>
                    <li><a href="#" class="btn btn-link">past events</a></li>
                    <li><a href="#" class="btn btn-link">our location</a></li>
                    <li><a href="#" class="btn btn-link">contact us</a></li>
                    <li><a href="#" class="btn btn-link">rht of companies</a></li>
                    <li><a href="#" class="btn btn-link">careers</a></li>
                </ul>
            </div>
            
            <div class="stay-connected col-xs-12 col-sm-12 col-md-3">
                <div class="title"> stay connected</div>
                <div class="social-media">
                    <a href="#"><i class="fa fa-pinterest-square"></i></a>
                    <a href="#"><i class="fa fa-linkedin-square"></i></a>
                    <a href="#"><i class="fa fa-twitter-square"></i></a>
                    <a href="#"><i class="fa fa-facebook-square"></i></a>
                    <a href="#"><i class="fa fa-google-plus-square"></i></a>
                </div>
                <div class="pt"> 
                <a href="#" class="btn btn-link">privacy</a> |
                <a href="#" class="btn btn-link">terms of use</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="left col-md-9"> RHT Academy Pte. Ltd. (UEN No. 201320120K) is incorporated under the Companies Act(Cap.50) in Singapore </div>
            <div class="right col-md-3"> &copy; 2015 RHT Academy Pte. Ltd</div>
        </div>
    </div>
</footer>