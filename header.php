<header>
  <!-- mobbile navbar -->
  <div class="visible-xs">
    <div class="top-nav well-sm">
        <a href="#" class="btn border-right"> subscribe for update <small><span class="glyphicon glyphicon-envelope"></span></small></a>
        <a href="#" class="btn border-right"> register </a>
        <a href="#" class="btn"> login </a>
    </div>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="#"><img src="images/header-logo.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">about us <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">aaa</a></li>
                <li><a href="#">sss</a></li>
                <li><a href="#">ddd</a></li>
              </ul>
            </li>
            <li><a href="#">seminar &events</a></li>
            <li><a href="#">past events</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="social-media">
              <a href="#"><i class="fa fa-pinterest-square"></i></a>
              <a href="#"><i class="fa fa-linkedin-square"></i></a>
              <a href="#"><i class="fa fa-twitter-square"></i></a>
              <a href="#"><i class="fa fa-facebook-square"></i></a>
              <a href="#"><i class="fa fa-google-plus-square"></i></a>
            </li>
            <li>
              <a href="#" class="btn btn-default search"><i class="fa fa-search"></i></a><a href="#" class="btn btn-default calendar"><i class="fa fa-calendar"></i> training calendar</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>

  <!-- tablet navbar -->
  <div class="hidden-xs">
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="#"><img src="images/header-logo.png"></a>
          </div>
          <div class="top-nav well-sm">
            <a href="#" class="btn border-right"> subscribe for update <small><span class="glyphicon glyphicon-envelope"></span></small></a>
            <a href="#" class="btn border-right"> register </a>
            <a href="#" class="btn"> login </a>
          </div>
          <div style="padding-top:50px">
            <ul class="nav navbar-nav navbar-left bottom-left">
              <li class="dropdown active">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">about us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">aaa</a></li>
                  <li><a href="#">sss</a></li>
                  <li><a href="#">ddd</a></li>
                </ul>
              </li>
              <li><a href="#">seminar &events</a></li>
              <li><a href="#">past events</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="social-media pull-right text-right">
                <a href="#"><i class="fa fa-pinterest-square"></i></a>
                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                <a href="#"><i class="fa fa-twitter-square"></i></a>
                <a href="#"><i class="fa fa-facebook-square"></i></a>
                <a href="#"><i class="fa fa-google-plus-square"></i></a>
              </li>
              <li class="pull-right" style="padding:11px">
                <a href="#" class="btn btn-default search"><i class="fa fa-search"></i></a><a href="#" class="btn btn-default calendar"><i class="fa fa-calendar"></i> training calendar</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>